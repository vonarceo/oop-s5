console.log("Von Arceo")

class Product{
    constructor(name, price){
        this.name = name;
        this.price = price;
        this.isActive = false;
    }

    archive(){
        if(this.isActive === false){
            this.isActive = true;
            console.log(`${this.name} is now Activated`)
            return this;
        }

        else {
            this.isActive = false;
            console.log(`${this.name} is now Deactivated`)
            return this;
        }
        
    }

    updatePrice(price){
        this.price = price
        console.log(`Successfully updated price of ${this.name} to ${price}`)
        return this;
    }

}

class Customer{
    constructor(email){
        this.email = email;
        this.cart = new Cart();
        this.orders = {
            products: [],
            totalAmount: 0
        }
    }

    
    checkOut(){
        this.cart.contents.forEach(product => this.orders.products.push(product))
        this.orders.totalAmount += this.cart.totalAmount
        this.cart.clearCartContents()
        return this
    }


}

class Cart{
    constructor(){
        this.contents = [];
        this.totalAmounts = 0;
    }

    addToCart(product, quantity){
        this.contents.push({
            product: product,
            quantity: quantity
        })
        

        return this;
    }

    showCartContents(){
        console.log(this.contents);
        return this;
    }
    
    clearCartContents(){
        this.contents = [];
        return this;
    }

    computeTotal(){
        if(this.contents.length == 0) return "Cart is empty"
        let total = 0
        this.contents.forEach(product => {
            total += product.quantity*product.product.price
        })
        this.totalAmount = total
        return this
    }

    updateProductQuantity(name, newQuantity){
        this.contents.find(product => product.product.name === name).quantity = newQuantity
    }

}



const prodA = new Product("Soap", 10);
const prodB = new Product("Laptop", 1000);

const von = new Customer("von@email.com");